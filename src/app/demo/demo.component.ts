import { Component, OnInit } from '@angular/core';

import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import {  FacebookLoginProvider } from 'angularx-social-login';


@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',    
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  user: SocialUser;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
    });
  }

 
//Call this method Sign in with facebook
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

 
// call this for sign out facebook
  signOut(): void {
    this.authService.signOut();
  }

}
