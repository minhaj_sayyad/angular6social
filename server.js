//Install express server
const express = require('express');
const path = require('path');

const app = express();


// Serve only the static files form the dist directory
app.use(express.static('./dist/fbassignment'));
app.listen(process.env.PORT || 8080);
app.get('/*', function(req,res) {
    
res.sendFile(path.join(__dirname+'/dist/fbassignment/index.html'));
console.log('server fb started');

});
